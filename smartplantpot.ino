#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // px
#define SCREEN_HEIGHT 64
#define OLED_RESET  -1
#define SCREEN_ADDRESS 0x3C // In documentation it should be 0x3D but for some reason it only works with this value which should be used for the 32px displays
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


#define LIGHT_SENSOR_PIN A1

#define EYE_WIDTH 12
#define EYE_LENGHT 40
#define EYE_SLEEP_LENGHT 3

//Eye states
#define EYE_STATE_NONE 0
#define EYE_STATE_MOVING 1
#define EYE_STATE_BLINK 2
#define EYE_STATE_SLEEP 3
#define EYE_STATE_SAD 4
#define EYE_STATE ANGRY 5
#define EYE_STATE HAPPY 6
#define EYE_STATE_DEAD 7
#define EYE_STATE_SLEEP_START 8
#define EYE_STATE_SLEEP_END 9

int currentEyeState = EYE_STATE_NONE;
int eyeCenterX = 48;
int eyeCenterY = 10;

#define INACTIVITY_LIMIT 5
int inactivityCounter = 0;


void SetupStateCheckInterupt()
{
    Serial.println("Start - Setting up interupts");
    // set timer1 interrupt at 1Hz
    TCCR1A = 0; // set entire TCCR1A register to 0
    TCCR1B = 0; // same for TCCR1B
    TCNT1 = 0;  // initialize counter value to 0
    // set compare match register for 1hz increments
    OCR1A = 35000; //about 2 secconds
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS12 and CS10 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
    Serial.println("Finished - Setting up interupts");
}

void SetupDisplay()
{
    Serial.println("Start - Setting up display");

    if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) 
    {
        Serial.println(F("SSD1306 allocation failed"));
        while(true)
        {
            // Don't proceed, loop forever
        } 
    }

    // initial render
    display.clearDisplay(); // get rid of adafruit logo that's already waiting in the framebuffer
    SadFace();
    //DrawFace();    
    display.display();

    Serial.println("Finished - Setting up display");
}

void EnsureRandomization()
{
    pinMode(A2, INPUT);
    delay(100);
    randomSeed(analogRead(A2));
}

void CheckInacivity()
{
    if (currentEyeState == EYE_STATE_NONE)
    {
        inactivityCounter++;
        Serial.print("Inactivity increasing: ");
        Serial.println(inactivityCounter);
    }
    else
    {
        Serial.println("Inactivity reset");
        inactivityCounter = 0;
    }

    if (inactivityCounter == INACTIVITY_LIMIT)
    {
        int idleAnimation = random(0, 100);

        if (idleAnimation > 50)
        {
            currentEyeState = EYE_STATE_MOVING;
        }
        else
        {
            currentEyeState = EYE_STATE_BLINK;
        }

        inactivityCounter = 0;
    }
}

void CheckBrigtness()
{
    int value = analogRead(LIGHT_SENSOR_PIN);
    if (value < 300 && currentEyeState != EYE_STATE_SLEEP && currentEyeState != EYE_STATE_SLEEP_START)
    {
        Serial.println("Sleep started");
        display.clearDisplay();
        currentEyeState = EYE_STATE_SLEEP_START;
    }
    else if (value > 300 && currentEyeState == EYE_STATE_SLEEP)
    {
        Serial.println("Sleep ended");
        currentEyeState = EYE_STATE_SLEEP_END;
    }
}

ISR(TIMER1_COMPA_vect)
{
  Serial.println("Interupt");
  CheckBrigtness();
  CheckInacivity();
}

void setup() {
    Serial.begin(115200);
    
    
    pinMode(LIGHT_SENSOR_PIN, INPUT);

    SetupStateCheckInterupt();
    SetupDisplay();
    EnsureRandomization();
}

void update_display()
{
    switch (currentEyeState)
    {
        case EYE_STATE_NONE:
            DrawFace();
            break;
        case EYE_STATE_BLINK:
            BlinkFace();
            RestEyeState();
            break;
        case EYE_STATE_MOVING:
            MoveFace();
            MoveFace();
            MoveFace();
            RestEyeState();
            break;
        case EYE_STATE_SLEEP:
            SleepFace();
            break;
        case EYE_STATE_SLEEP_START:
            SleepFaceStart();
            currentEyeState = EYE_STATE_SLEEP;
            break;
        case EYE_STATE_SLEEP_END:
            SleepFaceEnd();
            RestEyeState();
        case EYE_STATE_SAD:
            SadFace();
            break;
        default:
            break;
    }


    display.display();
}

void RestEyeState()
{
    currentEyeState = EYE_STATE_NONE;
}

void SleepFaceStart()
{
    for(int i = 0; i < EYE_LENGHT / 2; i += 3)
    {
        display.clearDisplay();
        display.fillRect(eyeCenterX - EYE_WIDTH, eyeCenterY + i, EYE_WIDTH, EYE_LENGHT - i * 2, SSD1306_WHITE);
        display.fillRect(eyeCenterX + EYE_WIDTH, eyeCenterY + i, EYE_WIDTH, EYE_LENGHT - i * 2, SSD1306_WHITE);
        display.display();
        delay(30);
    }

    display.setCursor(15, 1);
    display.setTextSize(2);             // Draw 2X-scale text
    display.setTextColor(SSD1306_WHITE);

    delay(30);
    display.println(F("ZZZ"));
}

void SleepFaceEnd()
{
    for(int i = EYE_LENGHT / 2; i > 0; i -= 3)
    {
        display.clearDisplay();
        display.fillRect(eyeCenterX - EYE_WIDTH, eyeCenterY + i, EYE_WIDTH, EYE_LENGHT - i * 2, SSD1306_WHITE);
        display.fillRect(eyeCenterX + EYE_WIDTH, eyeCenterY + i, EYE_WIDTH, EYE_LENGHT - i * 2, SSD1306_WHITE);
        display.display();
        delay(30);
    }
}

void SleepFace()
{
    // moves the ZZZ text created by the SleepFaceStart()
    display.startscrollright(0x00, 0x01);
    delay(1000);
    display.startscrollleft(0x00, 0x01);
    delay(1000);
    display.stopscroll();
}

void SadFace()
{
    eyeCenterX = 54;
    eyeCenterY = 10;
    display.clearDisplay();
    
    display.fillRect(eyeCenterX - EYE_WIDTH, eyeCenterY, EYE_WIDTH, EYE_LENGHT / 3, SSD1306_WHITE);
    // display.fillRect(eyeCenterX - EYE_WIDTH, eyeCenterY, 6, 12, SSD1306_BLACK); // tried to add more emotion to it, but it just looks weird
    display.fillRect(eyeCenterX + EYE_WIDTH, eyeCenterY, EYE_WIDTH, EYE_LENGHT / 3, SSD1306_WHITE);
    // display.fillRect(eyeCenterX + EYE_WIDTH, eyeCenterY, 6, 12, SSD1306_BLACK);

    display.display();
    int tearY = 18;
    delay(1000);
    display.fillRect(78, tearY, 5, 7, SSD1306_WHITE); // new tear position
    display.display();
    for(int i = 0; i < 32; i += 2)
    {
        display.fillRect(78, tearY, 5, 7, SSD1306_BLACK); // earse previous position
        tearY += i;
        display.fillRect(78, tearY, 5, 7, SSD1306_WHITE); // new tear position
        display.display();
        delay(100);        
    }
    
    delay(1000);
}

void DrawFace()
{
    display.fillRect(eyeCenterX - EYE_WIDTH, eyeCenterY, EYE_WIDTH, EYE_LENGHT, SSD1306_WHITE);
    display.fillRect(eyeCenterX + EYE_WIDTH, eyeCenterY, EYE_WIDTH, EYE_LENGHT, SSD1306_WHITE);    
}

void BlinkFace()
{
    SleepFaceStart();

    delay(100);

    SleepFaceEnd();
}

void MoveFace()
{
    int leftLimit = EYE_WIDTH * 2;
    int rightLimit = SCREEN_WIDTH - EYE_WIDTH * 2;
    int topLimit = 0;
    int bottomLimit = SCREEN_HEIGHT - EYE_LENGHT;

    int nextXPosition = random(leftLimit, rightLimit);
    int nextYPosition = random(topLimit, bottomLimit);

    int xAddition = eyeCenterX > nextXPosition ? -1 : 1;
    int yAddition = eyeCenterY > nextYPosition ? -1 : 1;

    while (eyeCenterX != nextXPosition && eyeCenterY != nextYPosition)
    {
        eyeCenterX += xAddition;
        eyeCenterY += yAddition;
        delay(50);

        display.clearDisplay();
        DrawFace();
        display.display();
    }

    display.clearDisplay();
    DrawFace();
    display.display();
}

void loop() {
    SadFace();
//   update_display();
}
